.PHONY: clean dirs

all: dirs ut_all main

ut_all: test/ut_all.cpp test/ut_transaction.h src/Transaction.cc test/ut_owed.h src/Owed.cc test/ut_lended.h src/Lended.cc test/ut_expenditure.h src/Expenditure.cc test/ut_income.h src/Income.cc test/ut_book.h src/Book.cc src/Menu.cc 
	g++ -o bin/ut_all test/ut_all.cpp src/Transaction.cc src/Owed.cc src/Lended.cc src/Expenditure.cc src/Income.cc src/Book.cc src/Menu.cc -lgtest -lpthread

main: ./main.cc src/Transaction.cc src/Owed.cc src/Lended.cc src/Expenditure.cc src/Income.cc src/Book.cc src/Menu.cc
	g++ -o bin/main ./main.cc src/Transaction.cc src/Owed.cc src/Lended.cc src/Expenditure.cc src/Income.cc src/Book.cc src/Menu.cc

clean:
	rm -rf ./bin

dirs:
	mkdir -p bin

stat:
	wc -l src/*
	wc -l test/*
