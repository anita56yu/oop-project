#ifndef _EXPENDITURE_H
#define _EXPENDITURE_H

#include <iostream>
#include "Transaction.h"

using namespace std;

enum CostType {food, transportation, entertainment, housing, cloth, otherCosts};

class Transaction;

class Expenditure: public Transaction{
public:

    Expenditure(string name, int yy, int mm, int dd, int amount, CostType type=otherCosts);
    
    void setType(CostType type);

    CostType type();

    virtual string print();

protected:

    CostType _type;

};

#endif