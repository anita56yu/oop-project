#include "Transaction.h"

Transaction::Transaction()
{
    _exist=false;
}

Transaction::Transaction(string name, int yy, int mm, int dd, int amount, TransactionTypes Ttype)
{
    _exist=true;
    _name=name;
    _year=yy;
    _month=mm;
    _day=dd;
    _amount=amount;
    _Ttype=Ttype;
}

void
Transaction::setName(string name){
    _name=name;
}

void
Transaction::setDetail(string detail)
{
    _detail=detail;
}

TransactionTypes
Transaction::Ttype(){
    return _Ttype;
}

bool
Transaction::exist()
{
    return _exist;
}

void
Transaction::setDate(int yy, int mm, int dd)
{
    _year=yy;
    _month=mm;
    _day=dd;
}

void
Transaction::setAmount(int amount)
{
    _amount=amount;
}

string
Transaction::name(){
    return _name;
}

int
Transaction::year()
{
    return _year;
}

int
Transaction::month()
{
    return _month;
}

int
Transaction::day()
{
    return _day;
}

int
Transaction::amount()
{
    return _amount;
}

string
Transaction::detail()
{
    if(_detail.empty())
        return string("No detail available.");
    return _detail;
}

bool dateValid(Transaction T)
{
    int yy=T.year();
    int mm=T.month();
    int dd=T.day();
    if(yy<0 || mm<=0 || mm>12 || dd<=0 || dd>31)
        return false;
    if((mm%2==1 && mm<=7) || (mm%2==0 && mm>=8)){
        if(dd>=1 && dd<=31)
            return true;
    }
    else if(mm==2 && yy%4==0){
        if(dd>=1 && dd<=29)
            return true;
    }
    else if(mm==2){
        if(dd>=1 && dd<=28)
            return true;
    }
    else{
        if(dd>=1 && dd<=30)
            return true;
    }
    return false;
}

string
Transaction::print()
{
    stringstream ss;
    ss<<TransactionTypesText[Ttype()]<<": "<<name()<<"  $"<<amount();
    return ss.str();

}