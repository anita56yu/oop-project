#include "Income.h"

Income::Income(string name, int yy, int mm, int dd, int amount, IncomeType type)
:Transaction(name, yy, mm, dd, amount, income)
{
    _type=type;
}

IncomeType
Income::type()
{
    return _type;
}

void
Income::setType(IncomeType type)
{
    _type=type;
}

string
Income::print(){
    stringstream ss;
    ss<<Transaction::print();
    return ss.str();
}
