#include <cstdlib>
#include <string>
#include "Menu.h"

string ChoiceText[]={"", "Exit", "Add an transaction", "Delete an transaction", "Edit an transaction", "Show Transaction by date", "Show amount by date"};
string TransactionTypesText1[]={"", "Owed", "Lended", "Income", "Expenditure"};

bool
Menu::mainMenu(int *choice)
{
    cout<<"\n-----------< Menu >-----------\n";
    for(int i=1; i<7; i++){
        cout<<" "<<i<<" ["<<ChoiceText[i]<<"]\n";
    }

    string input;
    int c=0,flag=0;
    do{
        if(flag==0) cout<<"\nEnter your choice: ";
	else cout<<"Choice not valid. Please enter again: ";
	cin>>c;
	flag++;
    }while(c<=0 || c>=7);

    cout<<"\n------------------------------\n";
    if(c == 1) return false;

    *choice = c;
    return true;
}

bool
Menu::dateValid(int yy, int mm, int dd)
{
    if(yy<0 || mm<=0 || mm>12 || dd<=0 || dd>31)
        return false;
    if((mm%2==1 && mm<=7) || (mm%2==0 && mm>=8)){
        if(dd>=1 && dd<=31)
            return true;
    }
    else if(mm==2 && yy%4==0){
        if(dd>=1 && dd<=29)
            return true;
    }
    else if(mm==2){
        if(dd>=1 && dd<=28)
            return true;
    }
    else{
        if(dd>=1 && dd<=30)
            return true;
    }
    return false;
}

Transaction* 
Menu::getTransaction(int type)
{        
    TransactionTypes c = getTranType(type);
    Transaction* temp;
    int yy, mm, dd, amount;
    string name, person;
    cout<<"\n[Enter a new transaction]\n";
    switch(c){
        case owed:
            cout<<"\nPlease enter [Name] [Year] [Month] [Day] [Amount] [Person]"<<endl;
            cin>>name>>yy>>mm>>dd>>amount>>person;
            temp=new Owed(name, yy, mm, dd, amount, person);
            break;
        case lended:
            cout<<"\nPlease enter [Name] [Year] [Month] [Day] [Amount] [Person]"<<endl;
            cin>>name>>yy>>mm>>dd>>amount>>person;
            temp=new Lended(name, yy, mm, dd, amount, person);
            break;
        case income:
            cout<<"\nPlease enter [Name] [Year] [Month] [Day] [Amount]"<<endl;
            cin>>name>>yy>>mm>>dd>>amount;
            temp=new Income(name, yy, mm, dd, amount);
            break;
        case expenditure:
            cout<<"\nPlease enter [Name] [Year] [Month] [Day] [Amount]"<<endl;
            cin>>name>>yy>>mm>>dd>>amount;
            temp=new Expenditure(name, yy, mm, dd, amount);
            break;
        default:
            break;
    }
    cout <<"Please enter detail: (if not please enter 0)"<<endl;
    string detail;
    cin >> detail;
    if(detail.compare("0")!=0)
        temp->setDetail(detail);
    return temp;
}

TransactionTypes
Menu::getTranType(int type)
{
    cout<<"\n[Select transaction type]\n";
    for(int i=0; i<type; i++){
        cout<<" "<<i+1<<" ["<<TransactionTypesText[i]<<"]\n";
    }
    string input;
    int c,flag=0;
    do{
        if(flag==0) cout<<"\nEnter your choice: ";
	else cout<<"Choice not valid. Please enter again: ";
	cin>>c;
	flag++;
    }while(c<=0 || c>type);

    switch(c){
	case 1: return owed;
	case 2: return lended;
	case 3: return income;
	case 4: return expenditure;
	case 5: return any;
    }
    return any;
}


bool
Menu::getDate(int *date)
{
    int yy,mm,dd;
    cout<<"\n[Choose the date of the transaction]\n";
    cout<<"(Entre any 0 if you want to cancel)\n";

    cout<<"Year: ";
    cin>> yy;
    if(yy == 0) return false;
    
    cout<<"Month: ";
    cin>> mm;
    if(mm == 0) return false;
    
    cout<<"Day: ";
    cin>> dd;
    if(dd == 0) return false;
    *date = yy*10000+mm*100+dd;

    if(!dateValid(yy,mm,dd)){
    	cout<< "Invalid Date!!\n";
	return false;
    }

    return true;
}

bool 
Menu::getDate(int *sdate, int *edate)
{
    int yys,mms,dds,yye,mme,dde;
    cout<<"\n[Choose the range of the date]\n";

    cout<<"Begin from (Enter any 0 if you want to cancel):\n";
    cout<<"Year: ";
    cin>> yys;
    if(yys == 0) return false;
    
    cout<<"Month: ";
    cin>> mms;
    if(mms == 0) return false;
    
    cout<<"Day: ";
    cin>> dds;
    if(dds == 0) return false;

    if(!dateValid(yys,mms,dds)){
    	cout<< "Invalid Date!!\n";
	return false;
    }

    cout<<"To (Enter any 0 if you want to cancel):\n";
    cout<<"Year: ";
    cin>> yye;
    if(yye == 0) return false;
    
    cout<<"Month: ";
    cin>> mme;
    if(mme == 0) return false;
    
    cout<<"Day: ";
    cin>> dde;
    if(dde == 0) return false;

    if(!dateValid(yye,mme,dde)){
    	cout<< "Invalid Date!!\n";
	return false;
    }

    *sdate = yys*10000+mms*100+dds;
    *edate = yye*10000+mme*100+dde;

    return true;
}

void 
Menu::printSum(int sdate, int edate, int sum, TransactionTypes type)
{
    cout<< sdate/10000 <<"/"<< sdate/100%100 <<"/"<< sdate%100 <<"-";
    cout<< edate/10000 <<"/"<< edate/100%100 <<"/"<< sdate%100 <<endl;
    cout<<TransactionTypesText[type]<<": $"<<sum<<"\n"<<endl;
}


int
Menu::getIndex()
{
    cout<<"\nPlease enter the index: ";
    int index;
    cin >> index;
    return index;
}

void
Menu::feedback(bool function){
    if(function==true)
        cout<<"\nAction succeeded!"<<endl;
    else
    	cout<<"\nAciton failed!"<<endl;
}
