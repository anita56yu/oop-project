#ifndef _TRANSACTION_H
#define _TRANSACTION_H

#include <iostream>
#include <sstream>

using namespace std;

enum TransactionTypes {owed, lended, income, expenditure, any};
static string TransactionTypesText[]={"Owed", "Lended", "Income", "Expenditure", "Any"};

class Transaction{
public:

    Transaction();

    Transaction(string name, int yy, int mm, int dd, int amount, TransactionTypes Ttype=any);

    virtual string print();

    void setName(string name);

    void setDetail(string detail);

    void setDate(int yy, int mm, int dd);

    void setAmount(int amount);

    string name();

    int year();
    
    int month();

    int day();

    int amount();

    string detail();

    TransactionTypes Ttype();

    bool exist();

    friend bool dateValid(Transaction T);

protected:
    int _year;

    int _month;

    int _day;

    int _amount;

    string _name;

    string _detail;

    bool _exist;

    TransactionTypes _Ttype;

};



#endif