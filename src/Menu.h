#ifndef _MENU_H
#define _MENU_H

#include <iostream>
#include "Transaction.h"
#include "Expenditure.h"
#include "Income.h"
#include "Lended.h"
#include "Owed.h"

using namespace  std;

class Menu{
public:
    bool mainMenu(int *choice);

    Transaction* getTransaction(int type=4);

    TransactionTypes getTranType(int type=5);

    bool dateValid(int yy, int mm, int dd);

    bool getDate(int *date);

    bool getDate(int *sdate, int *edate);

    void printSum(int sdate, int edate, int sum, TransactionTypes type);

    int getIndex();

    void feedback(bool function);

};






#endif
