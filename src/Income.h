#ifndef _INCOME_H
#define _INCOME_H

#include <iostream>
#include "Transaction.h"

using namespace std;

enum IncomeType {salaries, loteries, otherIncomes};

class Transaction;

class Income: public Transaction{
public:

    Income(string name, int yy, int mm, int dd, int amount, IncomeType type=otherIncomes);
    
    void setType(IncomeType type);

    IncomeType type();

    virtual string print();

protected:

    IncomeType _type;

};

#endif