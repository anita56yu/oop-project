#include "Lended.h"

Lended::Lended(string name, int yy, int mm, int dd, int amount, string person)
:Transaction(name, yy, mm, dd, amount, lended)
{
    _person=person;
}

string
Lended::person()
{
    return _person;
}

void
Lended::setPerson(string person)
{
    _person=person;
}

string
Lended::print(){
    stringstream ss;
    ss<<Transaction::print();
    ss<<" Lended to:"<<person();
    return ss.str();
}