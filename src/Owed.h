#ifndef _OWED_H
#define _OWED_H

#include <iostream>
#include "Transaction.h"

using namespace std;

class Transaction;

class Owed: public Transaction{
public:

    Owed(string name, int yy, int mm, int dd, int amount, string person);
    
    void setPerson(string person);

    string person();

    virtual string print();

protected:
    string _person;
};

#endif