#ifndef _BOOK_H
#define _BOOK_H

#include <iostream>
#include <list>
#include <map>
#include "Transaction.h"
#include "Expenditure.h"
#include "Income.h"
#include "Lended.h"
#include "Owed.h"
#include "Menu.h"

using namespace  std;

class Book{
public:
    Book();

    void mainLoop();

    bool addTransaction(Transaction *E);

    bool deleteTransaction(int date, TransactionTypes type, int index);

    void printTransactions(int startdate, int enddate, TransactionTypes type);
    
    void checkDetail(int startdate, int enddate, TransactionTypes type);

    bool editTransaction(int date, int index, Transaction *E, TransactionTypes type=any);

    int sum(int startdate, int enddate, TransactionTypes type=any);

    ~Book();

private:

    map <int, list<Transaction*> > _book;

    Menu _menu;
};





#endif
