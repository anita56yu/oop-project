#ifndef _LENDED_H
#define _LENDED_H

#include <iostream>
#include "Transaction.h"

using namespace std;

class Transaction;

class Lended: public Transaction{
public:

    Lended(string name, int yy, int mm, int dd, int amount, string person);
    
    void setPerson(string person);

    string person();

    virtual string print();

protected:
    string _person;
};

#endif