#include <string>
#include <fstream>
#include "Book.h"

using namespace std;

Book::Book(): _menu(Menu())
{
}

void 
Book::mainLoop()
{
    int choice = 1;
    while(_menu.mainMenu(&choice)){
        int sDate,eDate;
        TransactionTypes tran;

        switch(choice){
            case 2:
                _menu.feedback(addTransaction(_menu.getTransaction()));
                break;
            case 3:
                if(!_menu.getDate(&sDate)) break;
		        tran = _menu.getTranType();
                printTransactions(sDate, sDate, tran);
                _menu.feedback(deleteTransaction(sDate, tran, _menu.getIndex()));
                break;
            case 4:
                if(!_menu.getDate(&sDate)) break;
		        tran = _menu.getTranType();
                printTransactions(sDate, sDate, tran);
                _menu.feedback(editTransaction(sDate, _menu.getIndex(), _menu.getTransaction(), tran));
                break;
            case 5:
                if(!_menu.getDate(&sDate, &eDate)) break;
		        tran = _menu.getTranType();
                printTransactions(sDate, eDate, tran);
		        checkDetail(sDate,eDate,tran);
                break;
 	        case 6:
                if(!_menu.getDate(&sDate, &eDate)) break;
		        tran = _menu.getTranType();
                if(tran==owed || tran==any)
                    _menu.printSum(sDate, eDate, sum(sDate, eDate, owed), owed);
                if(tran==lended || tran==any)
                    _menu.printSum(sDate, eDate, sum(sDate, eDate, lended), lended);
                if(tran==income || tran==any)
                    _menu.printSum(sDate, eDate, sum(sDate, eDate, income), income);
                if(tran==expenditure || tran==any)
                    _menu.printSum(sDate, eDate, sum(sDate, eDate, expenditure), expenditure);
                break;
	    
        }
	
    }
}

bool
Book::addTransaction(Transaction *E)
{
    if(dateValid(*E)){
        map<int, list<Transaction*> >::iterator it;
        int date=E->year()*10000+E->month()*100+E->day();
        it= _book.find(date);
        if(it==_book.end())
            _book[date]=list<Transaction*>(1, E);
        else
            _book[date].insert(_book[date].end(), E);
        return true;
    }
    delete E;
    return false;
}

bool
Book::deleteTransaction(int date, TransactionTypes type, int index)
{
    if(index==0) return false;
    int counter=1;
    for(list<Transaction*>::iterator it=_book[date].begin(); it!=_book[date].end(); it++){
        if((type==any||type==(*it)->Ttype()) && counter==index){
            delete *it;
            _book[date].erase(it);
            return true;
        }
        else if((type==any||type==(*it)->Ttype())){
            counter++;
        }
    }
    return false;
}

void 
Book::printTransactions(int startdate, int enddate, TransactionTypes type)
{
    cout<<"\n------------------------";
    for(; startdate<=enddate; startdate++){
        int counter=1;
        for(list<Transaction*>::iterator it=_book[startdate].begin(); it!=_book[startdate].end(); it++){
            if(type==any||type==(*it)->Ttype()){
                if (counter==1)
                    cout<<"\n"<<startdate/10000<<"/"<<(startdate/100)%100<<"/"<<startdate%100<<endl;
                cout<<counter++<<"| "<<(*it)->print()<<endl;
            }
        }
    }
    cout<<"------------------------\n";
}

void 
Book::checkDetail(int startdate, int enddate, TransactionTypes type)
{
    cout<<"\n[Enter the date and the index to check the details]\n";
    
    int date, index;
    if(!_menu.getDate(&date)) return;
    if(date<startdate || date>enddate){
    	cout<<"This day is out of range. ";
	return;
    }
    index = _menu.getIndex();
    if(index==0) return;
    int counter=1;
    for(list<Transaction*>::iterator it=_book[date].begin(); it!=_book[date].end(); it++){
            if((type==any||type==(*it)->Ttype()) && counter==index){
    		cout<<"\n------------------------";
                cout<<"\n"<<counter++<<"| "<<(*it)->print()<<endl;
		if((*it)->detail()=="")
			cout<<"No details.\n";
		else
			cout<<"Details: "<<(*it)->detail()<<endl;
    		cout<<"------------------------\n";
		return;
            }
            else if((type==any||type==(*it)->Ttype())){
                counter++;
            }
    }
}

bool
Book::editTransaction(int date, int index, Transaction* E, TransactionTypes type)
{
    if(!deleteTransaction(date, type, index)){
        delete E;
        return false;
    }
    if(!addTransaction(E)){
        delete E;
        return false;
    }
    return true;
}

int 
Book::sum(int startdate, int enddate, TransactionTypes type)
{
    int sum=0;
    for(; startdate<=enddate; startdate++){
        for(list<Transaction*>::iterator lit = _book[startdate].begin(); lit!= _book[startdate].end(); lit++){
            if(type==(*lit)->Ttype())
                sum+=(*lit)->amount();
        }
    }
    return sum;
}


Book::~Book()
{
    map<int, list<Transaction*> >::iterator it=_book.begin();
    for(; it!=_book.end(); it++){
        list<Transaction*>::iterator lit=it->second.begin();
        for(; lit!=it->second.end(); lit++)
            delete *lit;
    }
}


