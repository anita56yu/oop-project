#include "Expenditure.h"

Expenditure::Expenditure(string name, int yy, int mm, int dd, int amount, CostType type)
:Transaction(name, yy, mm, dd, amount, expenditure)
{
    _type=type;
}

CostType
Expenditure::type()
{
    return _type;
}

void
Expenditure::setType(CostType type)
{
    _type=type;
}

string
Expenditure::print(){
    stringstream ss;
    ss<<Transaction::print();
    return ss.str();
}
