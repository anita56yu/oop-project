#include "Owed.h"

using namespace std;

Owed::Owed(string name, int yy, int mm, int dd, int amount, string person)
:Transaction(name, yy, mm, dd, amount, owed)
{
    _person=person;
}

string
Owed::person()
{
    return _person;
}

void
Owed::setPerson(string person)
{
    _person=person;
}

string
Owed::print(){
    stringstream ss;
    ss<<Transaction::print();
    ss<<" Owed to:"<<person();
    return ss.str();
}
