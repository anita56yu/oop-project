#include "../src/Income.h"

using namespace std;

TEST(IncomeTest, Constructor)
{
    Income A("Sep Salary", 2020, 10, 3, 200000);
    ASSERT_EQ(true, A.exist());
    ASSERT_EQ("Sep Salary", A.name());
    ASSERT_EQ(2020, A.year());
    ASSERT_EQ(10, A.month());
    ASSERT_EQ(3, A.day());
    ASSERT_EQ(200000, A.amount());
    ASSERT_EQ(otherIncomes, A.type());
    ASSERT_EQ("No detail available.", A.detail());
    ASSERT_EQ(income, A.Ttype());
}

TEST(IncomeTest, EditData)
{
    Income A("Sep Salary", 2020, 10, 3, 200000);
    ASSERT_EQ(true, A.exist());
    A.setName("Oct Salary");
    A.setDate(2019, 11, 2);
    A.setAmount(100000);
    A.setType(salaries);
    A.setDetail("FFF.");
    ASSERT_EQ("Oct Salary", A.name());
    ASSERT_EQ(2019, A.year());
    ASSERT_EQ(11, A.month());
    ASSERT_EQ(2, A.day());
    ASSERT_EQ(100000, A.amount());
    ASSERT_EQ(salaries, A.type());
    ASSERT_EQ("FFF.", A.detail());
    ASSERT_EQ(income, A.Ttype());
    ASSERT_EQ("Income: Oct Salary  $100000", A.print());
}
