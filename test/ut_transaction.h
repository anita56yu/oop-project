#include "../src/Transaction.h"

using namespace std;

TEST(TransactionTest, DefaultConstructor)
{
    Transaction A;
    ASSERT_EQ(false, A.exist());
}

TEST(TransactionTest, Constructor)
{
    Transaction A("Lunch", 2020, 12, 31, 200);
    ASSERT_EQ(true, A.exist());
    ASSERT_EQ("Lunch", A.name());
    ASSERT_EQ(2020, A.year());
    ASSERT_EQ(12, A.month());
    ASSERT_EQ(31, A.day());
    ASSERT_EQ(200, A.amount());
    ASSERT_EQ("No detail available.", A.detail());
    ASSERT_EQ(any, A.Ttype());
}

TEST(TransactionTest, EditData)
{
    Transaction A("Lunch", 2020, 12, 31, 200);
    ASSERT_EQ(true, A.exist());
    A.setName("Dinner");
    A.setDate(2019, 1, 2);
    A.setAmount(1000);
    A.setDetail("Pasta.");
    ASSERT_EQ("Dinner", A.name());
    ASSERT_EQ(2019, A.year());
    ASSERT_EQ(1, A.month());
    ASSERT_EQ(2, A.day());
    ASSERT_EQ(1000, A.amount());
    ASSERT_EQ("Pasta.", A.detail());
    ASSERT_EQ(any, A.Ttype());
}

TEST(TransactionTest, DateValid)
{
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2019, 2, 29, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", -1, 2, 3, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 1029, 0, 9, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2020, 2, 0, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2020, 4, 31, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2020, 6, 31, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2020, 9, 31, 200)));
    ASSERT_EQ(false, dateValid(Transaction("Lunch", 2020, 11, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 1, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 3, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 5, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 7, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 8, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 10, 31, 200)));
    ASSERT_EQ(true, dateValid(Transaction("Lunch", 2020, 12, 31, 200)));
}
