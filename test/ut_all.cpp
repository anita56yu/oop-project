#include <gtest/gtest.h>
#include "ut_transaction.h"
#include "ut_owed.h"
#include "ut_lended.h"
#include "ut_expenditure.h"
#include "ut_income.h"
#include "ut_book.h"

int main( int argc , char **argv )
{
    testing :: InitGoogleTest( &argc , argv ) ;
    return RUN_ALL_TESTS( ) ;
}
