#include "../src/Expenditure.h"

using namespace std;

TEST(ExpenditureTest, Constructor)
{
    Expenditure A("Lunch", 2020, 12, 31, 200);
    ASSERT_EQ(true, A.exist());
    ASSERT_EQ("Lunch", A.name());
    ASSERT_EQ(2020, A.year());
    ASSERT_EQ(12, A.month());
    ASSERT_EQ(31, A.day());
    ASSERT_EQ(200, A.amount());
    ASSERT_EQ(otherCosts, A.type());
    ASSERT_EQ("No detail available.", A.detail());
    ASSERT_EQ(expenditure, A.Ttype());
}

TEST(ExpenditureTest, EditData)
{
    Expenditure A("Lunch", 2020, 12, 31, 200);
    ASSERT_EQ(true, A.exist());
    A.setName("Dinner");
    A.setDate(2019, 1, 2);
    A.setAmount(1000);
    A.setType(food);
    A.setDetail("Pasta.");
    ASSERT_EQ("Dinner", A.name());
    ASSERT_EQ(2019, A.year());
    ASSERT_EQ(1, A.month());
    ASSERT_EQ(2, A.day());
    ASSERT_EQ(1000, A.amount());
    ASSERT_EQ(food, A.type());
    ASSERT_EQ("Pasta.", A.detail());
    ASSERT_EQ(expenditure, A.Ttype());
    ASSERT_EQ("Expenditure: Dinner  $1000", A.print());
}
