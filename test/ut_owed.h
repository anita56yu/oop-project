#include "../src/Owed.h"

using namespace std;

TEST(OwedTest, Constructor)
{
    Owed A("Lunch", 2020, 12, 31, 200, "Sara");
    ASSERT_EQ(true, A.exist());
    ASSERT_EQ("Lunch", A.name());
    ASSERT_EQ(2020, A.year());
    ASSERT_EQ(12, A.month());
    ASSERT_EQ(31, A.day());
    ASSERT_EQ(200, A.amount());
    ASSERT_EQ("Sara", A.person());
    ASSERT_EQ("No detail available.", A.detail());
    ASSERT_EQ(owed, A.Ttype());
}

TEST(OwedTest, EditData)
{
    Owed A("Lunch", 2020, 12, 31, 200, "Sara");
    ASSERT_EQ(true, A.exist());
    A.setName("Dinner");
    A.setDate(2019, 1, 2);
    A.setAmount(1000);
    A.setPerson("Anny");
    A.setDetail("Pasta.");
    ASSERT_EQ("Dinner", A.name());
    ASSERT_EQ(2019, A.year());
    ASSERT_EQ(1, A.month());
    ASSERT_EQ(2, A.day());
    ASSERT_EQ(1000, A.amount());
    ASSERT_EQ("Anny", A.person());
    ASSERT_EQ("Pasta.", A.detail());
    ASSERT_EQ(owed, A.Ttype());
    ASSERT_EQ("Owed: Dinner  $1000 Owed to:Anny", A.print());
}
