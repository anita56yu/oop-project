#include "../src/Book.h"

using namespace std;

TEST(BookTest, Constructor)
{
    Book A;
}

TEST(BookTest, AddTransaction)
{
    Book A;
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Breakfast", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Income("WIN", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Lended("Lunch", 2020, 12, 31, 200, "Anny")));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Lunch", 2020, 12, 31, 400)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 31, 800)));
}

TEST(BookTest, AddTransaction2)
{
    Book A;
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Breakfast", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Income("WIN", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Lended("Lunch", 2020, 12, 31, 200, "Anny")));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Lunch", 2020, 12, 31, 400)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 31, 800)));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 3));
    ASSERT_EQ(true, A.deleteTransaction(20201231, income, 1));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 1));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 1));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 1));
    A.addTransaction(new Expenditure("gyj", 2020, 12, 31, 200));
    //A.printTransactions(2020, 12, 31);
}

TEST(BookTest, DeleteTransaction)
{
    Book A;
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Breakfast", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Income("WIN", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Lended("Lunch", 2020, 12, 31, 200, "Anny")));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Lunch", 2020, 12, 31, 400)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 31, 800)));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 3));
    ASSERT_EQ(true, A.deleteTransaction(20201231, income, 1));
    //A.printTransactions(2020, 12, 31);
}

TEST(BookTest, EditTransaction)
{
    Book A;
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Breakfast", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Income("WIN", 2020, 12, 31, 200)));
    ASSERT_EQ(false, A.editTransaction(20201231, 1, new Income("WIN", 2020, 12, 31, 200), owed));
    ASSERT_EQ(true, A.editTransaction(20201231, 1, new Income("WIN!!!!", 2020, 12, 31, 200), any));
    ASSERT_EQ(true, A.editTransaction(20201231, 1, new Income("WIN!!!!", 2020, 12, 30, 200), any));
    ASSERT_EQ(true, A.deleteTransaction(20201231, any, 1));
    ASSERT_EQ(false, A.deleteTransaction(20201231, income, 1));
    //A.printTransactions(2020, 12, 31);
}

TEST(BookTest, SumTransaction2)
{
    Book A;
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Breakfast", 2020, 12, 31, 200)));
    ASSERT_EQ(true, A.addTransaction(new Income("WIN", 2020, 12, 31, 10000)));
    ASSERT_EQ(true, A.addTransaction(new Lended("Lunch", 2020, 12, 31, 700, "Anny")));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Lunch", 2020, 12, 31, 400)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 31, 800)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 11, 30, 800)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 25, 800)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 26, 800)));
    ASSERT_EQ(true, A.addTransaction(new Expenditure("Dinner", 2020, 12, 27, 800)));
    ASSERT_EQ(1400, A.sum(20201231, 20201231, expenditure));
    ASSERT_EQ(10000, A.sum(20201231, 20201231, income));
    ASSERT_EQ(700, A.sum(20201231, 20201231, lended));
    ASSERT_EQ(3200, A.sum(20201130, 20201227, expenditure));
}
